/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.Database;
import database.User;
import database.UserDao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werap
 */
public class LibraryProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        User newUser = new User();
        newUser.setUserId(-1);
        newUser.setLoginName("ABC");
        newUser.setPassword("password");
        newUser.setName("ABC");
        newUser.setSurname("DEF");
        newUser.setTypeId(1);
        UserDao.insert(newUser);
                
        
        ArrayList<User> list = UserDao.getUsers();
        for(User user:list) {
            System.out.println(user);
        }
        
        User user = UserDao.getUser(1);
        System.out.println(user);
      user.setName(user.getName()+"2");
        UserDao.update(user);
        
        user = UserDao.getUser(1);
        System.out.println(user);
        

        
        user = UserDao.getUser(3);
        System.out.println(user);
        UserDao.delete(user);
        user = UserDao.getUser(3);
    }

 
}
