/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import database.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Chanathip
 */
public class UserTableModel extends AbstractTableModel{
    ArrayList<User> userlist = new ArrayList<User>();
    String[] columnNames = {"#", "login name","name", "surname" ,"type"};
    @Override
    public int getRowCount() {
        return userlist.size();
        
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userlist.get(rowIndex);
        switch(columnIndex) {
            case 0: return  user.getUserId();
            case 1: return  user.getLoginName();
            case 2: return  user.getName();
            case 3: return  user.getSurname();
            case 4: return  user.getTypeId()==0?"Admin":"User";
        }
        return "";
    }
    
    public void setData(ArrayList<User> userList) {
        this.userlist = userList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    
}
